#define _CRTDBG_MAP_ALLOC
#include <iostream>
#include "includes/CExceptionDivisionZero.h"
#include "includes/CExceptionFileNotFound.h"
#include "includes/CExceptionFileSyntax.h"
#include "includes/CExceptionMismatchSizeMatrice.h"
#include "includes/CExceptionTypeMatrice.h"
#include "includes/CMatrice.h"
#include "includes/CMatriceAvancee.h"
#include "includes/CMatriceFichier.h"

int main(int argc, char *argv[])
{
	//if (argc <= 1) {
	//	std::cout << "Veuillez indiquer le chemin vers un fichier matrice en parametre" << std::endl;
	//	return 0;
	//}
	//try
	//{

	//CMatriceFichier* MFFichier = new CMatriceFichier();
	//CMatriceAvancee<double>** pMAmatrice = (CMatriceAvancee<double>**)malloc(sizeof(CMatriceAvancee<double>)*argc-1);
	//
	//for (int nBoucle = 0; nBoucle < argc - 1; nBoucle++) // constructions des matrice avec les fichiers
	//{
	//	MFFichier->MFSetFilePath(argv[nBoucle+1]);
	//	pMAmatrice[nBoucle] = MFFichier->MFGetMatriceAvanceeDouble();
	//	std::cout << "matrice " << nBoucle+1 << std::endl;
	//	pMAmatrice[nBoucle]->MAfficherMatrice();
	//	std::cout << std::endl;
	//}

	//std::cout << "les matrices ont ete cr�es" << std::endl;
	//std::cout << std::endl << "saisissez une valeur" << std::endl;
	//double dVal;
	//std::cin >> dVal;

	//for (int nBoucle = 0; nBoucle < argc - 1; nBoucle++)
	//{
	//	CMatriceAvancee<double>* MARes = pMAmatrice[nBoucle]->operator*(dVal);
	//	std::cout << "matrice " << nBoucle + 1 << " x " << dVal << " =" << std::endl;
	//	MARes->MAfficherMatrice();
	//	std::cout << std::endl;
	//	delete MARes;
	//}

	//for (int nBoucle = 0; nBoucle < argc - 1; nBoucle++)
	//{
	//	try
	//	{
	//		CMatriceAvancee<double>* MARes = pMAmatrice[nBoucle]->operator/(dVal);
	//		std::cout << "matrice " << nBoucle + 1 << " / " << dVal << " =" << std::endl;
	//		MARes->MAfficherMatrice();
	//		std::cout << std::endl;
	//		delete MARes;
	//	}
	//	catch (CExceptionDivisionZero* EDZObj)
	//	{
	//		EDZObj->EAfficherErreur();
	//	}
	//}

	//try
	//{
	//	CMatriceAvancee<double>* MAResAddition = new CMatriceAvancee<double>(pMAmatrice[0]);

	//	for (int nBoucle = 1; nBoucle < argc - 1; nBoucle++)
	//	{
	//		MAResAddition = pMAmatrice[nBoucle]->operator+(MAResAddition);
	//	}
	//	std::cout << "addition des matrices entre elles :" << std::endl;
	//	MAResAddition->MAfficherMatrice();
	//	std::cout << std::endl;
	//	delete MAResAddition;
	//}
	//catch (CExceptionMismatchSizeMatrice* EMSMObj)
	//{
	//	EMSMObj->EAfficherErreur();
	//}

	//try
	//{
	//	CMatriceAvancee<double>* MAResAddition = new CMatriceAvancee<double>(pMAmatrice[0]);

	//	for (int nBoucle = 1; nBoucle < argc - 1; nBoucle++)
	//	{
	//		if (nBoucle % 2 == 0)
	//			MAResAddition = pMAmatrice[nBoucle]->operator+(MAResAddition);
	//		else
	//			MAResAddition = pMAmatrice[nBoucle]->operator-(MAResAddition);
	//	}
	//	std::cout << "resultat de l operation suivante : M1-M2+M3-M4+M5-M6+... :" << std::endl;
	//	MAResAddition->MAfficherMatrice();
	//	std::cout << std::endl;
	//	delete MAResAddition;
	//}
	//catch (CExceptionMismatchSizeMatrice* EMSMObj)
	//{
	//	EMSMObj->EAfficherErreur();
	//}

	//try
	//{
	//	CMatriceAvancee<double>* MAResProduit = new CMatriceAvancee<double>(pMAmatrice[0]);

	//	for (int nBoucle = 1; nBoucle < argc - 1; nBoucle++)
	//	{
	//		MAResProduit = pMAmatrice[nBoucle]->operator*(MAResProduit);
	//	}
	//	std::cout << "resultat du produit des matrices :" << std::endl;
	//	MAResProduit->MAfficherMatrice();
	//	std::cout << std::endl;
	//	delete MAResProduit;
	//}
	//catch (CExceptionMismatchSizeMatrice* EMSMObj)
	//{
	//	EMSMObj->EAfficherErreur();
	//}

	//for (int nBoucle = 0; nBoucle < argc - 1; nBoucle++)
	//{
	//	delete pMAmatrice[nBoucle];
	//}
	//free(pMAmatrice);
	//}
	//catch (CExceptionFileNotFound* EFNFObj)
	//{
	//	EFNFObj->EAfficherErreur();
	//}
	//catch (CExceptionFileSyntax* EFSObj)
	//{
	//	EFSObj->EAfficherErreur();
	//}

	try
	{
		CMatriceFichier* MFFichier = new CMatriceFichier();
		MFFichier->MFSetFilePath(argv[1]);
		if (!strcmp(MFFichier->MFGetType(), "double"))
		{
			std::cout << "DOUBLE" << std::endl;
			CMatriceAvancee<double>* pMAmatrice = MFFichier->MFGetMatriceAvanceeDouble();
			pMAmatrice->MAfficherMatrice();
		}
		else if (!strcmp(MFFichier->MFGetType(), "float"))
		{
			std::cout << "FLOAT" << std::endl;
			CMatriceAvancee<float>* pMAmatrice = MFFichier->MFGetMatriceAvanceeFloat();
			pMAmatrice->MAfficherMatrice();
		}
		else if (!strcmp(MFFichier->MFGetType(), "int"))
		{
			std::cout << "INT" << std::endl;
			CMatriceAvancee<int>* pMAmatrice = MFFichier->MFGetMatriceAvanceeInt();
			pMAmatrice->MAfficherMatrice();
		}
		else  if (!strcmp(MFFichier->MFGetType(), "bool"))
		{
			std::cout << "BOOL" << std::endl;
			CMatriceAvancee<bool>* pMAmatrice = MFFichier->MFGetMatriceAvanceeBool();
			pMAmatrice->MAfficherMatrice();
		}
	}
	catch (CExceptionFileNotFound* EFNFObj)
	{
		EFNFObj->EAfficherErreur();
	}
	catch (CExceptionFileSyntax* EFSObj)
	{
		EFSObj->EAfficherErreur();
	}
	catch (CExceptionMismatchSizeMatrice* EMSMObj)
	{
		EMSMObj->EAfficherErreur();
	}
	catch (CExceptionDivisionZero* EDZObj)
	{
		EDZObj->EAfficherErreur();
	}
	
	return 0;
}