#ifndef C_EXCEPTION_FILE_NOT_FOUND_H
#define C_EXCEPTION_FILE_NOT_FOUND_H

#include "CException.h"

class CExceptionFileNotFound : public CException
{
private:

	char* pcEFNFMessage;

public:
	CExceptionFileNotFound();

	CExceptionFileNotFound(CExceptionFileNotFound* pEFNFArg);

	~CExceptionFileNotFound();

	void EAfficherErreur();

	void EFNFSetMessage(char* pcMessage);

	char* EFNFGetMessage();

};
#include "CExceptionFileNotFound.cpp"
#endif
