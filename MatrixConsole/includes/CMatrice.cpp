
/**
@brief Constructeur par defaut
@Param[in] -
*/
template<typename MType>
CMatrice<MType>::CMatrice() :ptMMat(NULL), nMNblignes(0), nMNbcolonnes(0) {}

/**
@brief Constructeur de copie
@Param[in] Objet pointeur CMatrice*
*/
template<typename MType>
CMatrice<MType>::CMatrice(CMatrice *pMArg)
{
	MSetmat(pMArg->ptMMat, pMArg->nMNblignes, pMArg->nMNbcolonnes);
}

/**
@brief Constructeur de CMatrice par fichier en entree
@Param[in] Chaine de caractere indiquant le fichier en lecture en entree
*/
template<class MType>
CMatrice<MType>::CMatrice(char * pcFilePath)
{
}

/**
@brief Destructeur
*/
template<typename MType>
CMatrice<MType>::~CMatrice()
{
	MType** ptPtr = MGetmat();
	for (unsigned int nBoucle = 0; nBoucle < MGetNblignes(); nBoucle++) free(ptPtr[nBoucle]);
	free(ptPtr);
}


/**
@brief Getter de nombre de lignes de l'objet CMatrice
*/
template<typename MType>
unsigned int CMatrice<MType>::MGetNblignes()
{
	return nMNblignes;
}

/**
@brief Getter de nombre de colonnes de l'objet CMatrice
*/
template<typename MType>
unsigned int CMatrice<MType>::MGetNbcolonnes()
{
	return nMNbcolonnes;
}

/**
@brief Getter d'objet CMatrice 
*/
template <class MType>
MType** CMatrice<MType>::MGetmat()
{
	return ptMMat;
}

/**
@brief Constructeur de CMatrice par fichier en entree
*/
template<typename MType>
void CMatrice<MType>::MSetNblignes(unsigned int nArg)
{
	nMNblignes = nArg;
}

/**
@brief Setter de nombre de colonnes de l'objet CMatrice
*/
template<typename MType>
void CMatrice<MType>::MSetNbcolonnes(unsigned int nArg)
{
	nMNbcolonnes = nArg;
}

/**
@brief Setter de l'objet CMatrice
@Param[in] Double pointeur representant une matrice generique
@Param[in] entier naturel representant le nombre de ligne de cette matrice
@Param[in] entier naturel representant le nombre de colonnes de cette matrice
@Param[out] -
*/
template <class MType>
void CMatrice<MType>::MSetmat(MType** ptArg, unsigned int nNbLignes, unsigned int nNbColomns)
{
	//allocation
	ptMMat = (MType **)malloc(nNbLignes * sizeof(MType*));

	   for (unsigned int nBoucle = 0; nBoucle < nNbLignes; nBoucle++)
		      ptMMat[nBoucle] = (MType *)malloc(nNbColomns * sizeof(MType));

	//initialisation
	for (unsigned int nBoucle1 = 0; nBoucle1 < nNbLignes; ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < nNbColomns; ++nBoucle2)
			ptMMat[nBoucle1][nBoucle2] = 0;


	//affectation de la nouvelle matrice
	for (unsigned int nBoucle1 = 0; nBoucle1 < nNbLignes; ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < nNbColomns; ++nBoucle2)
			ptMMat[nBoucle1][nBoucle2] = ptArg[nBoucle1][nBoucle2];

	MSetNblignes(nNbLignes);
	MSetNbcolonnes(nNbColomns);
}

/**
@brief Affichage de l'objet CMatrice
@Param[in] -
@Param[out] -
*/
template<typename MType>
void CMatrice<MType>::MAfficherMatrice()
{
	std::cout << "------------->Taille [" << MGetNblignes() << "][" << MGetNbcolonnes() << "]" << std::endl;
	for (unsigned int nBoucle1 = 0; nBoucle1 < MGetNblignes(); ++nBoucle1)
	{
		for (unsigned int nBoucle2 = 0; nBoucle2 < MGetNbcolonnes(); ++nBoucle2)
		{
			std::cout << ptMMat[nBoucle1][nBoucle2] << "\t";
		}
		std::cout << std::endl;
	}
}
