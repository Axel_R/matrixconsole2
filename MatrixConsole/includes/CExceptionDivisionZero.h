#ifndef C_EXCEPTION_DIVISION_ZERO_H
#define C_EXCEPTION_DIVISION_ZERO_H

#include "CException.h"

class CExceptionDivisionZero : public CException
{
private:

	char* pcEDZMessage;

public:
	CExceptionDivisionZero();

	CExceptionDivisionZero(CExceptionDivisionZero* pEDZArg);

	~CExceptionDivisionZero();

	void EAfficherErreur();

	void EDZSetMessage(char* pcMessage);

	char* EDZGettMessage();

};
#include "CExceptionDivisionZero.cpp"
#endif
