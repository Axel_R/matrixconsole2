/**
@brief Constructeur par defaut
@Param[in] -
*/
template<class MType>
CMatriceAvancee<MType>::CMatriceAvancee() : CMatrice<MType>() {}

/**
@brief Constructeur de copie
@Param[in] Objet pointeur CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>::CMatriceAvancee(CMatriceAvancee<MType>* pMAArg) : CMatrice<MType>(pMAArg) {}


/**
@brief Constructeur de CMatriceAvancee par fichier en entree
@Param[in] Chaine de caractere indiquant le fichier en lecture en entree
*/
template<class MType>
CMatriceAvancee<MType>::CMatriceAvancee(char* pcFilePath)
{

}


/**
@brief Operateur de division d'objet CMatriceAvancee sur objet variable generique
@Param[in] Variable generique
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator/(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* pMAResult = new CMatriceAvancee<MType>();

	// on traite l'exception de division sur 0
	if (MAArg == 0)
	{
		CExceptionDivisionZero* pEDZObjet = new CExceptionDivisionZero();
		pEDZObjet->EModifierCodeErreur(1);
		throw(pEDZObjet);
	}
	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] /= MAArg;
	pMAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return pMAResult;
}


/**
@brief Operateur de multiplication d'objet CMatriceAvancee fois objet variable generique
@Param[in] Variable generique
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator*(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* pMAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] *= pMAArg;
	pMAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return pMAResult;
}

/**
@brief Operateur de somme d'objet CMatriceAvancee avec objet variable generique
@Param[in] Variable generique
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator+(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* pMAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] += pMAArg;
	pMAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return pMAResult;
}

/**
@brief Operateur de soustraction d'objet CMatriceAvancee moins objet variable generique
@Param[in] Variable generique
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator-(MType MAArg)
{
	// On cree un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* pMAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] -= pMAArg;
	pMAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return pMAResult;
}
/**
@brief Methode de conversion d'une matrice de n'importe quel type � une matrice booleenne
@Param[in] -
@Param[out] Objet CMatriceAvancee* de type bool
*/
template<class MType>
CMatriceAvancee<bool>* CMatriceAvancee<MType>::ParseToBool()
{
	bool** pbMat = (bool**)malloc(this->MGetNblignes() * sizeof(bool*));

	MType** pMMat = this->MGetmat();

	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); ++nBoucle)
	{
		pbMat[nBoucle] = (bool*)malloc(this->MGetNbcolonnes() * sizeof(bool));
	}
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++) 
			pbMat[nBoucle1][nBoucle2] = (bool)pMMat[nBoucle1][nBoucle2]; // conversion des valeurs en type bool
	
	CMatriceAvancee<bool>* pMABool = new CMatriceAvancee<bool>();
	pMABool->MSetmat(pbMat, this->MGetNblignes() , this->MGetNbcolonnes() );
	return pMABool;
}

/**
@brief Generation de transposee d'objet CMatriceAvancee
@Param[in] -
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::MATranspose()
{
	
	//objet qui va contenir le rsultat 
	CMatriceAvancee<MType>* pMATranspose = new CMatriceAvancee<MType>();


	// pointeur pointe sur la matrice d'objet courant
	MType** pMAArg = this->MGetmat();

	// allocation de matrice temporaire 
	MType **pMTrans = (MType **)malloc(this->MGetNbcolonnes() * sizeof(MType*));
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNbcolonnes(); nBoucle++)
		pMTrans[nBoucle] = (MType *)malloc(this->MGetNblignes() * sizeof(MType));

	// matrice transpose
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
	{
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2) {
			pMTrans[nBoucle2][nBoucle1] = pMAArg[nBoucle1][nBoucle2];
		}
	}
	MATranspose->MSetmat(pMTrans,this->MGetNbcolonnes(),this->MGetNblignes());

	free(pMTrans);

	return pMATranspose;
}

/**
@brief Operateur de produit d'objet CMatriceAvancee fois objet CMatriceAvancee
@Param[in] Objet CMatriceAvancee
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator*(CMatriceAvancee<MType>* pMAArg)
{

	// Exception : non correspondance des matrices
	if (this->MGetNbcolonnes() != pMAArg->MGetNblignes())
	{
		CExceptionMismatchSizeMatrice* pEMSMObjet = new CExceptionMismatchSizeMatrice();

		pEMSMObjet->EModifierCodeErreur(1);

		throw(pEMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	MType** pMAmat1 = this->MGetmat();
	MType** pMAmat2 = pMAArg->MGetmat();

	// pointeur contiendra le resultat du produit
	MType **pMAMatResult = (MType **)malloc(this->MGetNblignes() * sizeof(MType*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		pMAMatResult[nBoucle] = (MType *)malloc(pMAArg->MGetNbcolonnes() * sizeof(MType));
	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < pMAArg->MGetNbcolonnes(); ++nBoucle2)
			     pMAMatResult[nBoucle1][nBoucle2] = 0;
	
	// operation de produit
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < pMAArg->MGetNbcolonnes(); ++nBoucle2)
			for (unsigned int nBoucle3 = 0; nBoucle3 < this->MGetNbcolonnes(); ++nBoucle3)   
				      pMAMatResult[nBoucle1][nBoucle2] += pMAmat1[nBoucle1][nBoucle3] * pMAmat2[nBoucle3][nBoucle2];

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de produit entre 2 objets du mme type
	CMatriceAvancee<MType>* pMAProduit = new CMatriceAvancee<MType>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	pMAProduit->MSetmat(pMAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete pMAMatResult;
	
	return pMAProduit;
}

/**
@brief Operateur de somme d'objet CMatriceAvancee plus objet CMatriceAvancee
@Param[in] Objet CMatriceAvancee
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator+(CMatriceAvancee* pMAArg)
{
	// Exception : non correspondance  entre les 2 matrices
	if ( ( this->MGetNblignes() != pMAArg->MGetNblignes() ) || ( this->MGetNbcolonnes() != pMAArg->MGetNbcolonnes() ) )
	{
		CExceptionMismatchSizeMatrice* pEMSMObjet = new CExceptionMismatchSizeMatrice();

		pEMSMObjet->EModifierCodeErreur(1);

		throw(pEMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	MType** pMAmat1 = this->MGetmat();
	MType** pMAmat2 = pMAArg->MGetmat();


	// pointeur contiendra le resultat de la somme
	MType **pMAMatResult = (MType **)malloc(this->MGetNblignes() * sizeof(MType*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		pMAMatResult[nBoucle] = (MType *)malloc(this->MGetNbcolonnes() * sizeof(MType));

	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			pMAMatResult[nBoucle1][nBoucle2] = 0;

	// operation 
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
				// operation de division et affectation des valeurs
				pMAMatResult[nBoucle1][nBoucle2] = pMAmat1[nBoucle1][nBoucle2] + pMAmat2[nBoucle1][nBoucle2];

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de produit entre 2 objets du mme type
	CMatriceAvancee<MType>* pMASomme = new CMatriceAvancee<MType>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	pMASomme->MSetmat(pMAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete pMAMatResult;

	return pMASomme;
}

/**
@brief Operateur de soustraction d'objet CMatriceAvancee moins objet CMatriceAvancee
@Param[in] Objet CMatriceAvancee
@Param[out] Objet CMatriceAvancee*
*/
template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator-(CMatriceAvancee* pMAArg)
{
	// Exception : non correspondance  entre les 2 matrices
	if ((this->MGetNblignes() != pMAArg->MGetNblignes()) || (this->MGetNbcolonnes() != pMAArg->MGetNbcolonnes()))
	{
		CExceptionMismatchSizeMatrice* pEMSMObjet = new CExceptionMismatchSizeMatrice();

		pEMSMObjet->EModifierCodeErreur(1);

		throw(pEMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	MType** pMAmat1 = this->MGetmat();
	MType** pMAmat2 = pMAArg->MGetmat();


	// pointeur contiendra le resultat de la somme
	MType **pMAMatResult = (MType **)malloc(this->MGetNblignes() * sizeof(MType*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		pMAMatResult[nBoucle] = (MType *)malloc(this->MGetNbcolonnes() * sizeof(MType));

	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			pMAMatResult[nBoucle1][nBoucle2] = 0;

	// operation 
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			// operation de division et affectation des valeurs
			pMAMatResult[nBoucle1][nBoucle2] = pMAmat1[nBoucle1][nBoucle2] + pMAmat2[nBoucle1][nBoucle2];

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de soustraction entre 2 objets du mme type
	CMatriceAvancee<MType>* pMASomme = new CMatriceAvancee<MType>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	pMASomme->MSetmat(pMAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete pMAMatResult;

	return pMASomme;
}

/**
@brief Methode d'operation booleenne "AND" (divison ou produit) d'objet CMatriceAvancee de type booleen avec une variable de type booleen
@Param[in] bool variable
@Param[out] Objet CMatriceAvancee* booleen
*/
template<class MType>
CMatriceAvancee<bool>* CMatriceAvancee<MType>::BoolProduitOuDivision(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<bool>* pMAResult = new CMatriceAvancee<bool>();

	// objet de copie 
	CMatriceAvancee<bool>* pMAAAide = this->ParseToBool();

	bool** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
		{
			// conversion des valeurs en type booleen en mode "AND"
			if (MAArg && pMAArg[nBoucle1][nBoucle2]) pMAArg[nBoucle1][nBoucle2] = true;
			else pMAArg[nBoucle1][nBoucle2] = false;
		}

	pMAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return pMAResult;
}


/**
@brief Operateur d'operation "OR" ( somme ou soustraction) d'objet CMatriceAvancee de type booleen avec une variable de type booleen
@Param[in] bool variable
@Param[out] Objet CMatriceAvancee* booleen
*/
template<class MType>
CMatriceAvancee<bool>* CMatriceAvancee<MType>::BoolSommeOuSoustraction(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<bool>* pMAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<bool>* pMAAAide = this->ParseToBool();

	bool** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
		{
			// conversion des valeurs en type booleen en mode "OR"
			if (MAArg || pMAArg[nBoucle1][nBoucle2]) pMAArg[nBoucle1][nBoucle2] = true;
			else pMAArg[nBoucle1][nBoucle2] = false;
		}

	pMAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return pMAResult;
}
/**
@brief Operateur d'operation "AND" (produit) d'objet CMatriceAvancee de type booleen avec un objet CMatriceAvancee
@Param[in] bool variable
@Param[out] Objet CMatriceAvancee* booleen
*/
template<class MType>
CMatriceAvancee<bool>* CMatriceAvancee<MType>::BoolProduit(CMatriceAvancee<MType>* pMAArg){

	// Exception : non correspondance des matrices
	if (this->MGetNbcolonnes() != pMAArg->MGetNblignes())
	{
		CExceptionMismatchSizeMatrice* pEMSMObjet = new CExceptionMismatchSizeMatrice();

		pEMSMObjet->EModifierCodeErreur(1);

		throw(pEMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	bool** pMAmat1 = this->ParseToBool()->MGetmat();
	bool** pMAmat2 = pMAArg->ParseToBool()->MGetmat();

	// pointeur contiendra le resultat du produit
	bool **pMAMatResult = (bool **)malloc(this->MGetNblignes() * sizeof(bool*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		pMAMatResult[nBoucle] = (bool *)malloc(pMAArg->MGetNbcolonnes() * sizeof(bool));
	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < pMAArg->MGetNbcolonnes(); ++nBoucle2)
			pMAMatResult[nBoucle1][nBoucle2] = 0;

	// operation de produit
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < pMAArg->MGetNbcolonnes(); ++nBoucle2)
			for (unsigned int nBoucle3 = 0; nBoucle3 < this->MGetNbcolonnes(); ++nBoucle3)
			{
				// conversion des valeurs en type booleen en mode "AND"
				if (pMAmat1[nBoucle1][nBoucle3] && pMAmat2[nBoucle3][nBoucle2]) pMAMatResult[nBoucle1][nBoucle2] = true;
				else pMAMatResult[nBoucle1][nBoucle2] = false;
			}

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de produit entre 2 objets du mme type
	CMatriceAvancee<bool>* pMAProduit = new CMatriceAvancee<bool>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	pMAProduit->MSetmat(pMAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete pMAMatResult;

	return pMAProduit;
}


/**
@brief Operateur d'operation "OR" (Somme ou Soustraction ) d'objet CMatriceAvancee de type booleen avec un objet CMatriceAvancee
@Param[in] bool variable
@Param[out] Objet CMatriceAvancee* booleen
*/
template<class MType>
CMatriceAvancee<bool>* CMatriceAvancee<MType>::BoolSommeOuSoustraction(CMatriceAvancee<MType>* pMAArg)
{
	// Exception : non correspondance  entre les 2 matrices
	if ((this->MGetNblignes() != pMAArg->MGetNblignes()) || (this->MGetNbcolonnes() != pMAArg->MGetNbcolonnes()))
	{
		CExceptionMismatchSizeMatrice* pEMSMObjet = new CExceptionMismatchSizeMatrice();

		pEMSMObjet->EModifierCodeErreur(1);

		throw(pEMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	bool** pMAmat1 = this->ParseToBool()->MGetmat();
	bool** pMAmat2 = pMAArg->ParseToBool()->MGetmat();


	// pointeur contiendra le resultat de la somme
	bool **pMAMatResult = (bool **)malloc(this->MGetNblignes() * sizeof(bool*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		pMAMatResult[nBoucle] = (bool *)malloc(this->MGetNbcolonnes() * sizeof(bool));

	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			pMAMatResult[nBoucle1][nBoucle2] = 0;

	// operation 
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
		{
			// conversion des valeurs en type booleen en mode "OR"
			if (pMAmat1[nBoucle1][nBoucle2] || pMAmat2[nBoucle1][nBoucle2]) pMAMatResult[nBoucle1][nBoucle2] = true;
			else pMAMatResult[nBoucle1][nBoucle2] = false;
		}

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de soustraction entre 2 objets du mme type
	CMatriceAvancee<bool>* pMASomme = new CMatriceAvancee<bool>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	pMASomme->MSetmat(pMAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete pMAMatResult;

	return pMASomme;
}