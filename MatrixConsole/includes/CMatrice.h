#ifndef C_MATRICE_H
#define C_MATRICE_H

#include <iostream>
#include <fstream>

template <class MType>
class CMatrice
{

private:

	MType** ptMMat;

	unsigned int nMNblignes;

	unsigned int nMNbcolonnes;

protected:
	void MSetNblignes(unsigned int nArg);
	void MSetNbcolonnes(unsigned int nArg);

public:
	CMatrice<MType>();

	CMatrice<MType>(CMatrice* pMArg);

	CMatrice<MType>(char* pcFilePath);

	~CMatrice<MType>();

	unsigned int MGetNblignes();

	unsigned int MGetNbcolonnes();

	MType** MGetmat();

	void MSetmat(MType** ptArg, unsigned int nNbLignes, unsigned int nNbColomns);

	void MAfficherMatrice();

};
#include "CMatrice.cpp"
#endif
