#include "CMatriceFichier.h"

CMatriceFichier::CMatriceFichier()
{
	pcMFFilePath = NULL;
}

CMatriceFichier::CMatriceFichier(CMatriceFichier& pMFArg)
{
	pcMFFilePath = pMFArg.MFGetFilePath();
}

CMatriceFichier::CMatriceFichier(char* pMFArg)
{
	pcMFFilePath = pMFArg;
}

CMatriceFichier::~CMatriceFichier()
{
	free(pcMFFilePath);
}

void CMatriceFichier::MFSetFilePath(char* pcArg)
{
	pcMFFilePath = pcArg;
}

char* CMatriceFichier::MFGetFilePath()
{
	return pcMFFilePath;
}

char* CMatriceFichier::MFGetType()
{
	if (!pcMFFilePath)
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	
	std::ifstream fichier(pcMFFilePath); //ouverture du fichier .txt 

	if (fichier)
	{

		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		for (int i = 0; cLine[i]; i++) { //lecture de la première ligne et on ignore la casse pour plus de flexibilité
			cLine[i] = tolower(cLine[i]);
		}
		
		if (!strcmp(cLine, "typematrice=double") || !strcmp(cLine, "typematrice=float") || !strcmp(cLine, "typematrice=int") || !strcmp(cLine, "typematrice=bool")) //on verifie que la syntax est bonne
		{
			char* cType = cLine + 12;
			return cType;
		}
		CExceptionFileSyntax* pEFSException = new CExceptionFileSyntax();
		pEFSException->EModifierCodeErreur(1);
		throw pEFSException;

	}
	else
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	return NULL;
}

CMatriceAvancee<double>* CMatriceFichier::MFGetMatriceAvanceeDouble()
{
	if (!pcMFFilePath)
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	
	std::ifstream fichier(pcMFFilePath); //ouverture du fichier .txt 

	if (fichier)
	{

		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		for (int i = 0; cLine[i]; i++) { //lecture de la première ligne et on ignore la casse pour plus de flexibilité
			cLine[i] = tolower(cLine[i]);
		}
		if (!strcmp(cLine, "typematrice=double")) //on verifie que la syntax est bonne
		{

			fichier.getline(cLine, 2048);
			char* cNbLines = new char[2048];
			strcpy_s(cNbLines, sizeof(cLine + 9), cLine + 9); 	//on sépare la deuxieme ligne en 2 parties
			cLine[9] = '\0';									//cline servira a verifier la syntaxe et l'autre partie pour construire l'objet matrice
			for (int i = 0; cLine[i]; i++) {
				cLine[i] = tolower(cLine[i]);
			}

			if (!strcmp(cLine, "nblignes="))
			{
				if (atoi(cNbLines) > 0) {
					fichier.getline(cLine, 2048);
					char* cNbCol = new char[2048]; //idem pour le nombre de colonnes
					strcpy_s(cNbCol, sizeof(cLine + 11), cLine + 11);

					cLine[11] = '\0';
					for (int i = 0; cLine[i]; i++) {
						cLine[i] = tolower(cLine[i]);
					}
					
					if (!strcmp(cLine, "nbcolonnes="))
					{
						if (atoi(cNbCol) > 0) {
							fichier.getline(cLine, 2048);
							for (int i = 0; cLine[i]; i++) {
								cLine[i] = tolower(cLine[i]);
							}

							if (!strcmp(cLine, "matrice=["))
							{
								char* cContext = new char[2048];
								
								double** mat = (double**)malloc(sizeof(double*) * atoi(cNbLines));

								for (size_t nBoucle = 0; nBoucle < 3; nBoucle++)
								{
									mat[nBoucle] = (double*)malloc(sizeof(double) * atoi(cNbCol));
								}
								
								for (int nBoucleLigne = 0; nBoucleLigne < atoi(cNbLines); nBoucleLigne++)
								{
									fichier.getline(cLine, 2048);
									for (int nBoucleCol = 0; nBoucleCol < atoi(cNbCol); nBoucleCol++)
									{
										strtok_s(cLine, " ", &cContext); // on contruit la matrice en isolant chaque nombre
										mat[nBoucleLigne][nBoucleCol] = atof(cLine);
										cLine = cContext;
									}
								}
								CMatriceAvancee<double>* matrice = new CMatriceAvancee<double>();
								matrice->MSetmat(mat, atoi(cNbLines), atoi(cNbCol));
								return matrice;
								//delete []cLine; //erreur "Invalid address specified to RtlValidateHeap" ?
								//delete []cContext;
								//delete []cNbCol;
								//delete []cNbLines;
							}
						}
					}

				}
			}

		}
		CExceptionFileSyntax* pEFSException = new CExceptionFileSyntax();
		pEFSException->EModifierCodeErreur(1);
		throw pEFSException;

	}
	else
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	return NULL;
}

CMatriceAvancee<int>* CMatriceFichier::MFGetMatriceAvanceeInt()
{
	if (!pcMFFilePath)
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	
	std::ifstream fichier(pcMFFilePath); //ouverture du fichier .txt 

	if (fichier)
	{

		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		for (int i = 0; cLine[i]; i++) { //lecture de la première ligne et on ignore la casse pour plus de flexibilité
			cLine[i] = tolower(cLine[i]);
		}
		if (!strcmp(cLine, "typematrice=int")) //on verifie que la syntax est bonne
		{
			fichier.getline(cLine, 2048);
			char* cNbLines = new char[2048];
			strcpy_s(cNbLines, sizeof(cLine + 9), cLine + 9); 	//on sépare la deuxieme ligne en 2 parties
			cLine[9] = '\0';									//cline servira a verifier la syntaxe et l'autre partie pour construire l'objet matrice
			for (int i = 0; cLine[i]; i++) {
				cLine[i] = tolower(cLine[i]);
			}

			if (!strcmp(cLine, "nblignes="))
			{
				if (atoi(cNbLines) > 0) {
					fichier.getline(cLine, 2048);
					char* cNbCol = new char[2048]; //idem pour le nombre de colonnes
					strcpy_s(cNbCol, sizeof(cLine + 11), cLine + 11);

					cLine[11] = '\0';
					for (int i = 0; cLine[i]; i++) {
						cLine[i] = tolower(cLine[i]);
					}
					
					if (!strcmp(cLine, "nbcolonnes="))
					{
						if (atoi(cNbCol) > 0) {
							fichier.getline(cLine, 2048);
							for (int i = 0; cLine[i]; i++) {
								cLine[i] = tolower(cLine[i]);
							}

							if (!strcmp(cLine, "matrice=["))
							{
								char* cContext = new char[2048];
								int** mat = (int**)malloc(sizeof(int*) * atoi(cNbLines));
								for (int nBoucle = 0; nBoucle < 3; nBoucle++)
								{
									mat[nBoucle] = (int*)malloc(sizeof(int) * atoi(cNbCol));
								}

								for (int nBoucleLigne = 0; nBoucleLigne < atoi(cNbLines); nBoucleLigne++)
								{
									fichier.getline(cLine, 2048);
									for (int nBoucleCol = 0; nBoucleCol < atoi(cNbCol); nBoucleCol++)
									{
										strtok_s(cLine, " ", &cContext); // on contruit la matrice en isolant chaque nombre
										mat[nBoucleLigne][nBoucleCol] = atoi(cLine);
										cLine = cContext;
									}
								}
								CMatriceAvancee<int>* matrice = new CMatriceAvancee<int>();
								matrice->MSetmat(mat, atoi(cNbLines), atoi(cNbCol));
								return matrice;
								//delete []cLine; //erreur "Invalid address specified to RtlValidateHeap" ?
								//delete []cContext;
								//delete []cNbCol;
								//delete []cNbLines;
							}
						}
					}

				}
			}

		}
		CExceptionFileSyntax* pEFSException = new CExceptionFileSyntax();
		pEFSException->EModifierCodeErreur(1);
		throw pEFSException;

	}
	else
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	return NULL;
}

CMatriceAvancee<float>* CMatriceFichier::MFGetMatriceAvanceeFloat()
{
	if (!pcMFFilePath)
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	
	std::ifstream fichier(pcMFFilePath); //ouverture du fichier .txt 

	if (fichier)
	{

		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		for (int i = 0; cLine[i]; i++) { //lecture de la première ligne et on ignore la casse pour plus de flexibilité
			cLine[i] = tolower(cLine[i]);
		}
		if (!strcmp(cLine, "typematrice=float")) //on verifie que la syntax est bonne
		{

			fichier.getline(cLine, 2048);
			char* cNbLines = new char[2048];
			strcpy_s(cNbLines, sizeof(cLine + 9), cLine + 9); 	//on sépare la deuxieme ligne en 2 parties
			cLine[9] = '\0';									//cline servira a verifier la syntaxe et l'autre partie pour construire l'objet matrice
			for (int i = 0; cLine[i]; i++) {
				cLine[i] = tolower(cLine[i]);
			}

			if (!strcmp(cLine, "nblignes="))
			{
				if (atoi(cNbLines) > 0) {
					fichier.getline(cLine, 2048);
					char* cNbCol = new char[2048]; //idem pour le nombre de colonnes
					strcpy_s(cNbCol, sizeof(cLine + 11), cLine + 11);

					cLine[11] = '\0';
					for (int i = 0; cLine[i]; i++) {
						cLine[i] = tolower(cLine[i]);
					}
					
					if (!strcmp(cLine, "nbcolonnes="))
					{
						if (atoi(cNbCol) > 0) {
							fichier.getline(cLine, 2048);
							for (int i = 0; cLine[i]; i++) {
								cLine[i] = tolower(cLine[i]);
							}

							if (!strcmp(cLine, "matrice=["))
							{
								char* cContext = new char[2048];
								float** mat = (float**)malloc(sizeof(float*) * atoi(cNbLines));

								for (size_t nBoucle = 0; nBoucle < 3; nBoucle++)
								{
									mat[nBoucle] = (float*)malloc(sizeof(float) * atoi(cNbCol));
								}
								for (int nBoucleLigne = 0; nBoucleLigne < atoi(cNbLines); nBoucleLigne++)
								{
									fichier.getline(cLine, 2048);
									for (int nBoucleCol = 0; nBoucleCol < atoi(cNbCol); nBoucleCol++)
									{
										strtok_s(cLine, " ", &cContext); // on contruit la matrice en isolant chaque nombre

										mat[nBoucleLigne][nBoucleCol] = (float)atof(cLine);
										cLine = cContext;
									}
								}
								CMatriceAvancee<float>* matrice = new CMatriceAvancee<float>();
								matrice->MSetmat(mat, atoi(cNbLines), atoi(cNbCol));
								return matrice;
								//delete []cLine; //erreur "Invalid address specified to RtlValidateHeap" ?
								//delete []cContext;
								//delete []cNbCol;
								//delete []cNbLines;
							}
						}
					}

				}
			}

		}
		CExceptionFileSyntax* pEFSException = new CExceptionFileSyntax();
		pEFSException->EModifierCodeErreur(1);
		throw pEFSException;

	}
	else
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	return NULL;
}

CMatriceAvancee<bool>* CMatriceFichier::MFGetMatriceAvanceeBool()
{
	if (!pcMFFilePath)
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	
	std::ifstream fichier(pcMFFilePath); //ouverture du fichier .txt 

	if (fichier)
	{

		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		for (int i = 0; cLine[i]; i++) { //lecture de la première ligne et on ignore la casse pour plus de flexibilité
			cLine[i] = tolower(cLine[i]);
		}
		if (!strcmp(cLine, "typematrice=bool")) //on verifie que la syntax est bonne
		{

			fichier.getline(cLine, 2048);
			char* cNbLines = new char[2048];
			strcpy_s(cNbLines, sizeof(cLine + 9), cLine + 9); 	//on sépare la deuxieme ligne en 2 parties
			cLine[9] = '\0';									//cline servira a verifier la syntaxe et l'autre partie pour construire l'objet matrice
			for (int i = 0; cLine[i]; i++) {
				cLine[i] = tolower(cLine[i]);
			}

			if (!strcmp(cLine, "nblignes="))
			{
				if (atoi(cNbLines) > 0) {
					fichier.getline(cLine, 2048);
					char* cNbCol = new char[2048]; //idem pour le nombre de colonnes
					strcpy_s(cNbCol, sizeof(cLine + 11), cLine + 11);

					cLine[11] = '\0';
					for (int i = 0; cLine[i]; i++) {
						cLine[i] = tolower(cLine[i]);
					}
					
					if (!strcmp(cLine, "nbcolonnes="))
					{
						if (atoi(cNbCol) > 0) {
							fichier.getline(cLine, 2048);
							for (int i = 0; cLine[i]; i++) {
								cLine[i] = tolower(cLine[i]);
							}

							if (!strcmp(cLine, "matrice=["))
							{
								char* cContext = new char[2048];
								bool** mat = (bool**)malloc(sizeof(bool*) * atoi(cNbLines));

								for (size_t nBoucle = 0; nBoucle < 3; nBoucle++)
								{
									mat[nBoucle] = (bool*)malloc(sizeof(bool) * atoi(cNbCol));
								}
								for (int nBoucleLigne = 0; nBoucleLigne < atoi(cNbLines); nBoucleLigne++)
								{
									fichier.getline(cLine, 2048);
									for (int nBoucleCol = 0; nBoucleCol < atoi(cNbCol); nBoucleCol++)
									{
										strtok_s(cLine, " ", &cContext); // on contruit la matrice en isolant chaque nombre
										if (!strcmp(cLine, "true"))
										{
											mat[nBoucleLigne][nBoucleCol] = true;
										}
										else if (!strcmp(cLine, "false"))
										{
											mat[nBoucleLigne][nBoucleCol] = false;
										}
										else
										{
											CExceptionTypeMatrice* pETMException = new CExceptionTypeMatrice();
											pETMException->EModifierCodeErreur(1);
											throw pETMException;
										}
										cLine = cContext;
									}
								}
								CMatriceAvancee<bool>* matrice = new CMatriceAvancee<bool>();
								matrice->MSetmat(mat, atoi(cNbLines), atoi(cNbCol));
								return matrice;
								//delete []cLine; //erreur "Invalid address specified to RtlValidateHeap" ?
								//delete []cContext;
								//delete []cNbCol;
								//delete []cNbLines;
							}
						}
					}

				}
			}

		}
		CExceptionFileSyntax* pEFSException = new CExceptionFileSyntax();
		pEFSException->EModifierCodeErreur(1);
		throw pEFSException;

	}
	else
	{
		CExceptionFileNotFound* pEFNFException = new CExceptionFileNotFound();
		pEFNFException->EModifierCodeErreur(1);
		throw pEFNFException;
	}
	return NULL;
}