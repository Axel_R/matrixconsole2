#ifndef C_MATRICE_FICHIER_H
#define C_MATRICE_FICHIER_H

#include "CMatriceAvancee.h"

class CMatriceFichier
{
private:

	char* pcMFFilePath;

public:
	CMatriceFichier();

	CMatriceFichier(CMatriceFichier& pMFArg);

	CMatriceFichier(char* pMFArg);

	~CMatriceFichier();

	void MFSetFilePath(char* pcArg);

	char* MFGetFilePath();

	char* MFGetType();

	CMatriceAvancee<double>* MFGetMatriceAvanceeDouble();

	CMatriceAvancee<int>* MFGetMatriceAvanceeInt();

	CMatriceAvancee<float>* MFGetMatriceAvanceeFloat();

	CMatriceAvancee<bool>* MFGetMatriceAvanceeBool();

};
#include "CMatriceFichier.cpp"
#endif
